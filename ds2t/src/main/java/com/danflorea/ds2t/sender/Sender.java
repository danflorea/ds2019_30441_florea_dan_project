package com.danflorea.ds2t.sender;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

@Component
public class Sender implements CommandLineRunner {

    private final static String QUEUE_NAME = "SALVE";

    @Override
    public void run(String... args) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");

        String fileName = "D:\\Real stuff\\DS\\ds2t\\ds2t\\src\\main\\java\\com\\danflorea\\ds2t\\sender\\inp.txt";
        List<PatientData> patientDataList = new ArrayList<>();


        try (Stream<String> stream = Files.lines((Paths.get(fileName))))
        {
            stream.forEach(
                    line->{
                        String[] parts = line.split("\t\t");
                        PatientData pd = new PatientData();


                        String s1 = parts[0].replaceAll("-", "/");
                        String s2 = parts[1].replaceAll("-", "/");

                        try
                        {
                            SimpleDateFormat st1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                            Date dateStart = st1.parse(s1);
                            Long dateStartMillis = dateStart.getTime();

                            SimpleDateFormat st2 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                            Date dateEnd = st2.parse(s2);
                            Long dateEndMillis = dateEnd.getTime();

                            Random rand = new Random();
                            Integer r = rand.nextInt();
                            pd.setPatient_id(r.toString());
                            pd.setActivity(parts[2]);
                            pd.setStartTime(dateStartMillis);
                            pd.setEndTime(dateEndMillis);

                            patientDataList.add(pd);
                        }
                        catch (Exception e)
                        {
                            System.out.println("Date time thing croaked");
                        }
                    }
            );

        }
        catch (IOException e)
        {
            System.out.println("Ooooooooooops");
        }

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel())
        {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            String message = patientDataList.toString();
            channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
            System.out.println(" [x] Sent '" + message + "'");
        }
    }


}
