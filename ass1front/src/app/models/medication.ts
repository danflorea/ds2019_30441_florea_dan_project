export class Medication {
    public mid: number;
    public name: string;
    public side_effects: string;

    constructor (
        name?: string,
        side_effects?: string
    ){
        this.name = name;
        this.side_effects = side_effects;
    }
}