import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpParams } from '@angular/common/http';
import { Product } from 'src/app/tab1/products';
import { Observable } from 'rxjs';
import { finalize, catchError } from 'rxjs/operators';
import { MedUser } from 'src/app/models/medUser';
import { ReducedUserInfo } from 'src/app/models/reducedUserInfo';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

	configUrl = 'http://localhost:8080/';
	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

	constructor(private http: HttpClient) { }

	getConfig(url?): Observable<any>{
		return this.http.get(this.configUrl + url);
	}

	postConfig(data: any, url?:string){
		if(url==null){
			return this.http.post(this.configUrl, data);
		}
		else{
			return this.http.post(this.configUrl+url, data);
		}
	}

	putConfig(data: any, url?:string) {
		return this.http.put(this.configUrl+url, data, this.httpOptions);
	}

	deleteConfig(data: number, url?:string) {
		let dest = this.configUrl+url+'/'+data;

		return this.http.delete(dest, this.httpOptions); 
	}

}
