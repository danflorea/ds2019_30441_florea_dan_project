package com.danf.dsproject.persistence.jpa;

import com.danf.dsproject.entities.Medication;
import com.danf.dsproject.persistence.api.MedicationRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.Repository;

public interface JpaMedicationRepository extends Repository<Medication, Integer>, MedicationRepository {

    void delete(Medication element);

    @Override
    default void remove (Medication element) {
        delete(element);
    }
}
