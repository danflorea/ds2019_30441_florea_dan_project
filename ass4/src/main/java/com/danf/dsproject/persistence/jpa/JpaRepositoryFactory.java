package com.danf.dsproject.persistence.jpa;

import com.danf.dsproject.persistence.api.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class JpaRepositoryFactory implements RepositoryFactory {

    private final JpaMedUserRepository medUserRepository;
    private final JpaMedicationPlanRepository medicationPlanRepository;
    private final JpaMedicationRepository medicationRepository;
    private final JpaPatientDataRepository patientDataRepository;

    @Override
    public MedicationPlanRepository createMedicationPlanRepository() {
        return medicationPlanRepository;
    }

    @Override
    public MedicationRepository createMedicationRepository() {
        return medicationRepository;
    }

    @Override
    public JpaMedUserRepository createMedUserRepository() {
        return medUserRepository;
    }

    @Override
    public JpaPatientDataRepository createPatientDataRepository() {
        return patientDataRepository;
    }


}
