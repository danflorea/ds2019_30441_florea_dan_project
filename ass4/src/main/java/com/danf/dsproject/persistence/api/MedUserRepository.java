package com.danf.dsproject.persistence.api;

import com.danf.dsproject.entities.MedUser;

import java.util.List;
import java.util.Optional;

public interface MedUserRepository {

    MedUser save (MedUser medUser);
    Optional<MedUser> findById(Integer id);
    List<MedUser> findAll();
    void remove(MedUser medUser);



}
