package com.danf.dsproject.persistence.api;

import com.danf.dsproject.persistence.jpa.JpaMedUserRepository;
import com.danf.dsproject.persistence.jpa.JpaPatientDataRepository;

public interface RepositoryFactory {

    MedicationPlanRepository createMedicationPlanRepository();
    MedicationRepository createMedicationRepository();
    JpaMedUserRepository createMedUserRepository();
    JpaPatientDataRepository createPatientDataRepository();

}
