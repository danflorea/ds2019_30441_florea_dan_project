package com.danf.dsproject.controllers;

import com.danf.dsproject.grpcImpl.MedicationPlanService;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class GrpcServerController implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        Server server = ServerBuilder
                .forPort(6565)
                .addService(new MedicationPlanService())
                .build();
        server.start();

        System.out.println("Server started at " + server.getPort());

        server.awaitTermination();
    }
}
