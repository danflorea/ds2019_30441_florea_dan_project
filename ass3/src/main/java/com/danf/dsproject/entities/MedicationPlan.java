package com.danf.dsproject.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class MedicationPlan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer planId;


    @ManyToMany
    @JoinTable(
        name="med_plan",
        joinColumns=@JoinColumn(name="planId"),
        inverseJoinColumns=@JoinColumn(name="medId")
    )
    private List<Medication> medication;

    private Double dosage;
    private String instructions;

    @ManyToOne
    private MedUser patient;

}
