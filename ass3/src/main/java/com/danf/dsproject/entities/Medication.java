package com.danf.dsproject.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@ToString(of = {"mid", "name", "side_effects"})
@AllArgsConstructor
@NoArgsConstructor
public class Medication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer mid;

    private String name;
    private String side_effects;

    @ManyToMany
    @JoinTable(
        name="med_plan",
        joinColumns=@JoinColumn(name="medId"),
        inverseJoinColumns=@JoinColumn(name="planId")
    )
    private List<MedicationPlan> medicationPlans;


    public Medication(String name, String side_effects) {
        this.name = name;
        this.side_effects = side_effects;
    }
}
