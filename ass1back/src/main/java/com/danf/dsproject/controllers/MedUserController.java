package com.danf.dsproject.controllers;

import com.danf.dsproject.dto.CompleteMedUserDTO;
import com.danf.dsproject.dto.MedUserDTO;
import com.danf.dsproject.dto.ReducedUserInfoDTO;
import com.danf.dsproject.entities.MedUser;
import com.danf.dsproject.service.MedUserManagementService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequiredArgsConstructor
public class MedUserController {

    private final MedUserManagementService userManagementService;


    @GetMapping("/users")
    public List<MedUserDTO> readAll() {
        return userManagementService.findAll().stream().map(u -> MedUserDTO.ofEntity(u)).collect(Collectors.toList());
    }

    private MedUser medUserDTOtoMedUser(MedUserDTO dto) {
        return new MedUser(
                dto.getName(),
                dto.getGender(),
                dto.getBirthdate(),
                dto.getAddress(),
                dto.getUsername(),
                dto.getPassword(),
                dto.getRole());
    }

    @GetMapping("/user/{id}")
    public List<MedUserDTO> getPatients (@PathVariable("id") Integer id)
    {
        return userManagementService.getPatientsOfCaregiver(id);
    }

    @PostMapping("/users")
    public MedUserDTO create(@RequestBody MedUserDTO dto) {
        return dto.ofEntity(userManagementService.save(medUserDTOtoMedUser(dto)));
    }


    @PutMapping("/users")
    public void update (@RequestBody MedUserDTO dto) {
        Optional<MedUser> user = userManagementService.findUserById(dto.getUid());
        if (user.isPresent()) {
            user.get().setName(dto.getName());
            user.get().setGender(dto.getGender());
            user.get().setBirthdate(dto.getBirthdate());
            user.get().setAddress(dto.getAddress());
            user.get().setUsername(dto.getUsername());
            user.get().setPassword(dto.getPassword());
            user.get().setRole(dto.getRole());
            userManagementService.save(user.get());
        }
    }

    @DeleteMapping("/users/{id}")
    public String delete (@PathVariable("id") Integer id) {
        Optional<MedUser> user = userManagementService.findUserById(id);
        userManagementService.delete(user.get());
        return "Haha yes";
    }

}
